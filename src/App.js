import React, { Component } from 'react';
import Sidebar from "react-sidebar";
import carticon from "./cart-icon.png"
import Pagination from "./components/pagination"
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sidebarOpen: false,
      products: [],
      pagesNumber: 0,
      currentPage: 0,
      cart: [],
    };
  }

  componentDidMount() {
    fetch("https://jsonplaceholder.typicode.com/photos")
      .then(response => response.json())
      .then(data => this.setState({ products: data, pagesNumber: Math.ceil(data.length / 15)}));
  }

  onCartClick = () => {
      this.setState({ sidebarOpen: !this.state.sidebarOpen });  
  }

  onCloseCartClick = () => {
    this.setState({ sidebarOpen: false });
  }

  onAddClick = item => {
    if (!this.state.cart.find(cart_item => cart_item.id === item.id)) {
      this.setState((state) => {
        return ({ cart: state.cart.concat(item) });
      })
    }
  }

  onDeleteClick = item => {
    this.setState(state => {
      return ({ cart: state.cart.filter(cart_item => cart_item.id === item.id ? false : true) });
    });
  }
  
  goToPage = page => {
    this.setState({ currentPage: page - 1 });
  }

  render() {
    return (
      <div>
        <Sidebar
          sidebar={
          <div className="cart-container">
          <button className="btn close-cart" onClick={this.onCloseCartClick}><h3>X</h3></button>
            <h3 className="cart-title">Panier ({this.state.cart.length})</h3>
            <div>
              {
                this.state.cart.map((item) => {
                  return (
                    <div className="product-item" key={item.id}>
                      <img className="product-img" src={item.url} alt="img"/>
                      <div>{item.title} <button onClick={() => this.onDeleteClick(item)} title="Retirer du panier" className="btn">(-)</button></div>
                    </div>
                  );
                })
              }
            </div>
          </div>}
          docked={this.state.sidebarOpen}
          styles={{ sidebar: { background: "white", width: "300px" } }}
        >
          <div>
            {
              !this.state.sidebarOpen ? <button
                title={(!this.state.sidebarOpen ? "Ouvrir" : "Fermer") + " le panier"}
                className="btn" onClick={this.onCartClick}>
                  <img width={30} src={carticon} alt="CART"/>
                </button> : null
            }
          </div>
        </Sidebar>
        <div className="products-list">
          <Pagination currentPage={this.state.currentPage + 1} pagesNumber={this.state.pagesNumber} goToPage={this.goToPage} />
          {
            this.state.products.slice(this.state.currentPage * 15, this.state.currentPage * 15 + 15)
              .map(item => {
                return (
                  <div className="product-item" key={item.id}>
                    <img className="product-img" src={item.url} alt="img"/>
                    <div className="product-title">{item.title}</div> <button className="btn product-button" onClick={() => this.onAddClick(item)} title="Ajouter au panier">(+)</button>
                  </div>
                );
              })
          }
        <Pagination currentPage={this.state.currentPage + 1} pagesNumber={this.state.pagesNumber} goToPage={this.goToPage} />
        </div>
      </div>
    );
  }
}

export default App;
