import PropTypes from 'prop-types';
import React from 'react';
import cn from 'classnames';
import './pagination.css';  


export default class Pagination extends React.PureComponent {
  static propTypes = {
    pagesNumber: PropTypes.number.isRequired,
    currentPage: PropTypes.number.isRequired,
    goToPage: PropTypes.func.isRequired,
  }

  initList = () => {
    let ret = [];
    let count = 1;
    if (this.props.currentPage > 6 && this.props.pagesNumber - this.props.currentPage > 7) {
      count = this.props.currentPage - 7;
    } else if (this.props.pagesNumber - this.props.currentPage < 8) {
      count = this.props.pagesNumber - 14;
    }
    for (let index = 0; index < 15; index++) {
      ret[index] = count++;
    }
    return ret;
  }

  render() {
    let page_list = this.initList();

    return (
      <ul className="pagination">
        <li>
          <button className="btn" onClick={() => this.props.goToPage(1)}>
            <div>&laquo; &laquo;</div>
          </button>
        </li>
        <li>
          <button
            className="btn"
            onClick={() => {
              if (this.props.currentPage !== 1) {
                this.props.goToPage(this.props.currentPage - 1);
              }
            }
          }>
            <div>&laquo;</div>
          </button>
        </li>
        {
          page_list.map(page => {
            return (
              <li key={page} onClick={() => this.props.goToPage(page)}
                  className={cn({active: page === this.props.currentPage - 1})}><button className="btn">{page}</button>
              </li>
            );
          })
        }
        <li>
          <button
            className="btn"
            onClick={() => {
              if (this.props.currentPage !== this.props.pagesNumber) {
                this.props.goToPage(this.props.currentPage + 1);
              }
            }
          }>
            <span>&raquo;</span>
          </button>
        </li>
        <li>
          <button className="btn" onClick={() => this.props.goToPage(this.props.pagesNumber)}>
            <span>&raquo; &raquo;</span>
          </button>
        </li>
      </ul>
    );
  }
}
