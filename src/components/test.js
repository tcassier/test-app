import React, { Component } from "react";
import cn from 'classnames';

export default class Pagination extends Component {
  render () {
    return (
      <nav>
        <ul>
        <li>
          <button className={cn({disabled: this.props.activePage === 1})}
              onClick={() => {
                if (this.props.activePage !== 1) {
                  this.props.goToPage(this.props.activePage - 1);
                }
              }}>
            <span>&laquo;</span>
          </button>
        </li>
        </ul>
      </nav>
    );
  }
}